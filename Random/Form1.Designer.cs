﻿namespace Random
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.comboBoxFunction = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panelMACGenerator = new System.Windows.Forms.Panel();
            this.checkBoxIncludeColon = new System.Windows.Forms.CheckBox();
            this.textBoxMACOutput = new System.Windows.Forms.TextBox();
            this.buttonGenerateMAC = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPrefix = new System.Windows.Forms.TextBox();
            this.panelPhoneNumberGenerator = new System.Windows.Forms.Panel();
            this.checkBoxIncludeHyphen = new System.Windows.Forms.CheckBox();
            this.checkBoxIncludePlus1 = new System.Windows.Forms.CheckBox();
            this.textBoxPhoneOutput = new System.Windows.Forms.TextBox();
            this.buttonGeneratePhone = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxAreaCode = new System.Windows.Forms.TextBox();
            this.panelPasswordGenerator = new System.Windows.Forms.Panel();
            this.checkBoxIncludeAt = new System.Windows.Forms.CheckBox();
            this.checkBoxIncludeSpecialCharacter = new System.Windows.Forms.CheckBox();
            this.textBoxPasswordOutput = new System.Windows.Forms.TextBox();
            this.buttonGeneratePassword = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxPasswordLength = new System.Windows.Forms.TextBox();
            this.labelAlertPasssword = new System.Windows.Forms.Label();
            this.labelAlertPhoneNumber = new System.Windows.Forms.Label();
            this.labelAlertMacAddress = new System.Windows.Forms.Label();
            this.panelMACGenerator.SuspendLayout();
            this.panelPhoneNumberGenerator.SuspendLayout();
            this.panelPasswordGenerator.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxFunction
            // 
            this.comboBoxFunction.DisplayMember = "Password";
            this.comboBoxFunction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFunction.FormattingEnabled = true;
            this.comboBoxFunction.Location = new System.Drawing.Point(61, 111);
            this.comboBoxFunction.Name = "comboBoxFunction";
            this.comboBoxFunction.Size = new System.Drawing.Size(277, 21);
            this.comboBoxFunction.TabIndex = 1;
            this.comboBoxFunction.SelectedIndexChanged += new System.EventHandler(this.ComboBoxFunction_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Options";
            // 
            // panelMACGenerator
            // 
            this.panelMACGenerator.Controls.Add(this.labelAlertMacAddress);
            this.panelMACGenerator.Controls.Add(this.checkBoxIncludeColon);
            this.panelMACGenerator.Controls.Add(this.textBoxMACOutput);
            this.panelMACGenerator.Controls.Add(this.buttonGenerateMAC);
            this.panelMACGenerator.Controls.Add(this.label1);
            this.panelMACGenerator.Controls.Add(this.textBoxPrefix);
            this.panelMACGenerator.Location = new System.Drawing.Point(15, 218);
            this.panelMACGenerator.Name = "panelMACGenerator";
            this.panelMACGenerator.Size = new System.Drawing.Size(326, 98);
            this.panelMACGenerator.TabIndex = 3;
            // 
            // checkBoxIncludeColon
            // 
            this.checkBoxIncludeColon.AutoSize = true;
            this.checkBoxIncludeColon.Location = new System.Drawing.Point(109, 8);
            this.checkBoxIncludeColon.Name = "checkBoxIncludeColon";
            this.checkBoxIncludeColon.Size = new System.Drawing.Size(76, 17);
            this.checkBoxIncludeColon.TabIndex = 7;
            this.checkBoxIncludeColon.Text = "inlcude \":\"";
            this.checkBoxIncludeColon.UseVisualStyleBackColor = true;
            // 
            // textBoxMACOutput
            // 
            this.textBoxMACOutput.Location = new System.Drawing.Point(94, 53);
            this.textBoxMACOutput.Name = "textBoxMACOutput";
            this.textBoxMACOutput.ReadOnly = true;
            this.textBoxMACOutput.Size = new System.Drawing.Size(229, 20);
            this.textBoxMACOutput.TabIndex = 6;
            this.textBoxMACOutput.DoubleClick += new System.EventHandler(this.TextBoxMACOutput_DoubleClick);
            // 
            // buttonGenerateMAC
            // 
            this.buttonGenerateMAC.Location = new System.Drawing.Point(3, 51);
            this.buttonGenerateMAC.Name = "buttonGenerateMAC";
            this.buttonGenerateMAC.Size = new System.Drawing.Size(75, 23);
            this.buttonGenerateMAC.TabIndex = 3;
            this.buttonGenerateMAC.Text = "Generate";
            this.buttonGenerateMAC.UseVisualStyleBackColor = true;
            this.buttonGenerateMAC.Click += new System.EventHandler(this.ButtonGenerateMAC_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Prefix";
            // 
            // textBoxPrefix
            // 
            this.textBoxPrefix.Location = new System.Drawing.Point(42, 6);
            this.textBoxPrefix.Name = "textBoxPrefix";
            this.textBoxPrefix.Size = new System.Drawing.Size(46, 20);
            this.textBoxPrefix.TabIndex = 2;
            // 
            // panelPhoneNumberGenerator
            // 
            this.panelPhoneNumberGenerator.Controls.Add(this.labelAlertPhoneNumber);
            this.panelPhoneNumberGenerator.Controls.Add(this.checkBoxIncludeHyphen);
            this.panelPhoneNumberGenerator.Controls.Add(this.checkBoxIncludePlus1);
            this.panelPhoneNumberGenerator.Controls.Add(this.textBoxPhoneOutput);
            this.panelPhoneNumberGenerator.Controls.Add(this.buttonGeneratePhone);
            this.panelPhoneNumberGenerator.Controls.Add(this.label4);
            this.panelPhoneNumberGenerator.Controls.Add(this.textBoxAreaCode);
            this.panelPhoneNumberGenerator.Location = new System.Drawing.Point(106, 146);
            this.panelPhoneNumberGenerator.Name = "panelPhoneNumberGenerator";
            this.panelPhoneNumberGenerator.Size = new System.Drawing.Size(326, 98);
            this.panelPhoneNumberGenerator.TabIndex = 4;
            // 
            // checkBoxIncludeHyphen
            // 
            this.checkBoxIncludeHyphen.AutoSize = true;
            this.checkBoxIncludeHyphen.Location = new System.Drawing.Point(142, 9);
            this.checkBoxIncludeHyphen.Name = "checkBoxIncludeHyphen";
            this.checkBoxIncludeHyphen.Size = new System.Drawing.Size(76, 17);
            this.checkBoxIncludeHyphen.TabIndex = 8;
            this.checkBoxIncludeHyphen.Text = "inlcude \"-\"";
            this.checkBoxIncludeHyphen.UseVisualStyleBackColor = true;
            // 
            // checkBoxIncludePlus1
            // 
            this.checkBoxIncludePlus1.AutoSize = true;
            this.checkBoxIncludePlus1.Location = new System.Drawing.Point(225, 9);
            this.checkBoxIncludePlus1.Name = "checkBoxIncludePlus1";
            this.checkBoxIncludePlus1.Size = new System.Drawing.Size(85, 17);
            this.checkBoxIncludePlus1.TabIndex = 7;
            this.checkBoxIncludePlus1.Text = "inlcude \"+1\"";
            this.checkBoxIncludePlus1.UseVisualStyleBackColor = true;
            // 
            // textBoxPhoneOutput
            // 
            this.textBoxPhoneOutput.Location = new System.Drawing.Point(94, 53);
            this.textBoxPhoneOutput.Name = "textBoxPhoneOutput";
            this.textBoxPhoneOutput.ReadOnly = true;
            this.textBoxPhoneOutput.Size = new System.Drawing.Size(229, 20);
            this.textBoxPhoneOutput.TabIndex = 6;
            this.textBoxPhoneOutput.DoubleClick += new System.EventHandler(this.TextBoxPhoneOutput_DoubleClick);
            // 
            // buttonGeneratePhone
            // 
            this.buttonGeneratePhone.Location = new System.Drawing.Point(3, 51);
            this.buttonGeneratePhone.Name = "buttonGeneratePhone";
            this.buttonGeneratePhone.Size = new System.Drawing.Size(75, 23);
            this.buttonGeneratePhone.TabIndex = 3;
            this.buttonGeneratePhone.Text = "Generate";
            this.buttonGeneratePhone.UseVisualStyleBackColor = true;
            this.buttonGeneratePhone.Click += new System.EventHandler(this.ButtonGeneratePhone_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Area Code";
            // 
            // textBoxAreaCode
            // 
            this.textBoxAreaCode.Location = new System.Drawing.Point(66, 6);
            this.textBoxAreaCode.Name = "textBoxAreaCode";
            this.textBoxAreaCode.Size = new System.Drawing.Size(62, 20);
            this.textBoxAreaCode.TabIndex = 2;
            // 
            // panelPasswordGenerator
            // 
            this.panelPasswordGenerator.Controls.Add(this.labelAlertPasssword);
            this.panelPasswordGenerator.Controls.Add(this.checkBoxIncludeAt);
            this.panelPasswordGenerator.Controls.Add(this.checkBoxIncludeSpecialCharacter);
            this.panelPasswordGenerator.Controls.Add(this.textBoxPasswordOutput);
            this.panelPasswordGenerator.Controls.Add(this.buttonGeneratePassword);
            this.panelPasswordGenerator.Controls.Add(this.label5);
            this.panelPasswordGenerator.Controls.Add(this.textBoxPasswordLength);
            this.panelPasswordGenerator.Location = new System.Drawing.Point(12, 12);
            this.panelPasswordGenerator.Name = "panelPasswordGenerator";
            this.panelPasswordGenerator.Size = new System.Drawing.Size(326, 98);
            this.panelPasswordGenerator.TabIndex = 5;
            // 
            // checkBoxIncludeAt
            // 
            this.checkBoxIncludeAt.AutoSize = true;
            this.checkBoxIncludeAt.Location = new System.Drawing.Point(150, 9);
            this.checkBoxIncludeAt.Name = "checkBoxIncludeAt";
            this.checkBoxIncludeAt.Size = new System.Drawing.Size(59, 17);
            this.checkBoxIncludeAt.TabIndex = 8;
            this.checkBoxIncludeAt.Text = "only @";
            this.checkBoxIncludeAt.UseVisualStyleBackColor = true;
            // 
            // checkBoxIncludeSpecialCharacter
            // 
            this.checkBoxIncludeSpecialCharacter.AutoSize = true;
            this.checkBoxIncludeSpecialCharacter.Location = new System.Drawing.Point(225, 9);
            this.checkBoxIncludeSpecialCharacter.Name = "checkBoxIncludeSpecialCharacter";
            this.checkBoxIncludeSpecialCharacter.Size = new System.Drawing.Size(85, 17);
            this.checkBoxIncludeSpecialCharacter.TabIndex = 7;
            this.checkBoxIncludeSpecialCharacter.Text = "Special char";
            this.checkBoxIncludeSpecialCharacter.UseVisualStyleBackColor = true;
            // 
            // textBoxPasswordOutput
            // 
            this.textBoxPasswordOutput.Location = new System.Drawing.Point(94, 53);
            this.textBoxPasswordOutput.Name = "textBoxPasswordOutput";
            this.textBoxPasswordOutput.ReadOnly = true;
            this.textBoxPasswordOutput.Size = new System.Drawing.Size(229, 20);
            this.textBoxPasswordOutput.TabIndex = 6;
            this.textBoxPasswordOutput.DoubleClick += new System.EventHandler(this.TextBoxPasswordOutput_DoubleClick);
            // 
            // buttonGeneratePassword
            // 
            this.buttonGeneratePassword.Location = new System.Drawing.Point(3, 51);
            this.buttonGeneratePassword.Name = "buttonGeneratePassword";
            this.buttonGeneratePassword.Size = new System.Drawing.Size(75, 23);
            this.buttonGeneratePassword.TabIndex = 3;
            this.buttonGeneratePassword.Text = "Generate";
            this.buttonGeneratePassword.UseVisualStyleBackColor = true;
            this.buttonGeneratePassword.Click += new System.EventHandler(this.ButtonGeneratePassword_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Length";
            // 
            // textBoxPasswordLength
            // 
            this.textBoxPasswordLength.Location = new System.Drawing.Point(66, 6);
            this.textBoxPasswordLength.Name = "textBoxPasswordLength";
            this.textBoxPasswordLength.Size = new System.Drawing.Size(62, 20);
            this.textBoxPasswordLength.TabIndex = 2;
            // 
            // labelAlertPasssword
            // 
            this.labelAlertPasssword.AutoSize = true;
            this.labelAlertPasssword.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAlertPasssword.ForeColor = System.Drawing.Color.Red;
            this.labelAlertPasssword.Location = new System.Drawing.Point(95, 35);
            this.labelAlertPasssword.Name = "labelAlertPasssword";
            this.labelAlertPasssword.Size = new System.Drawing.Size(0, 13);
            this.labelAlertPasssword.TabIndex = 9;
            // 
            // labelAlertPhoneNumber
            // 
            this.labelAlertPhoneNumber.AutoSize = true;
            this.labelAlertPhoneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAlertPhoneNumber.ForeColor = System.Drawing.Color.Red;
            this.labelAlertPhoneNumber.Location = new System.Drawing.Point(98, 36);
            this.labelAlertPhoneNumber.Name = "labelAlertPhoneNumber";
            this.labelAlertPhoneNumber.Size = new System.Drawing.Size(0, 13);
            this.labelAlertPhoneNumber.TabIndex = 10;
            // 
            // labelAlertMacAddress
            // 
            this.labelAlertMacAddress.AutoSize = true;
            this.labelAlertMacAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAlertMacAddress.ForeColor = System.Drawing.Color.Red;
            this.labelAlertMacAddress.Location = new System.Drawing.Point(98, 37);
            this.labelAlertMacAddress.Name = "labelAlertMacAddress";
            this.labelAlertMacAddress.Size = new System.Drawing.Size(0, 13);
            this.labelAlertMacAddress.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(355, 152);
            this.Controls.Add(this.panelPasswordGenerator);
            this.Controls.Add(this.panelPhoneNumberGenerator);
            this.Controls.Add(this.panelMACGenerator);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxFunction);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.panelMACGenerator.ResumeLayout(false);
            this.panelMACGenerator.PerformLayout();
            this.panelPhoneNumberGenerator.ResumeLayout(false);
            this.panelPhoneNumberGenerator.PerformLayout();
            this.panelPasswordGenerator.ResumeLayout(false);
            this.panelPasswordGenerator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBoxFunction;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelMACGenerator;
        private System.Windows.Forms.CheckBox checkBoxIncludeColon;
        private System.Windows.Forms.TextBox textBoxMACOutput;
        private System.Windows.Forms.Button buttonGenerateMAC;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPrefix;
        private System.Windows.Forms.Panel panelPhoneNumberGenerator;
        private System.Windows.Forms.CheckBox checkBoxIncludePlus1;
        private System.Windows.Forms.TextBox textBoxPhoneOutput;
        private System.Windows.Forms.Button buttonGeneratePhone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxAreaCode;
        private System.Windows.Forms.CheckBox checkBoxIncludeHyphen;
        private System.Windows.Forms.Panel panelPasswordGenerator;
        private System.Windows.Forms.CheckBox checkBoxIncludeAt;
        private System.Windows.Forms.CheckBox checkBoxIncludeSpecialCharacter;
        private System.Windows.Forms.TextBox textBoxPasswordOutput;
        private System.Windows.Forms.Button buttonGeneratePassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxPasswordLength;
        private System.Windows.Forms.Label labelAlertPasssword;
        private System.Windows.Forms.Label labelAlertMacAddress;
        private System.Windows.Forms.Label labelAlertPhoneNumber;
    }
}

