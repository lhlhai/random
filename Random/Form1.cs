﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Random
{
    public partial class Form1 : Form
    {
        List<string> listFunc = new List<string>();
        string name = " Generator";

        string PASSWORD_CHARS_LCASE = "abcdefghijklmnopqrstuvwxyz";
        string PASSWORD_CHARS_UCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        string PASSWORD_CHARS_NUMERIC = "0123456789";
        string PASSWORD_CHARS_SPECIAL = "!#$%^&*?_~-£()";
        string PASSWORD_CHAR_AT = "@";

        public Form1()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            listFunc.Add("Password");
            listFunc.Add("MAC Address");
            listFunc.Add("Phone Number");

            //Update new function to listFunc

            textBoxPasswordOutput.Cursor = Cursors.Hand;
            textBoxMACOutput.Cursor = Cursors.Hand;
            textBoxPhoneOutput.Cursor = Cursors.Hand;

            textBoxPasswordLength.Text = 10.ToString();

            checkBoxIncludeAt.Checked = true;
            checkBoxIncludeSpecialCharacter.Checked = true;

            foreach (string str in listFunc)
            {
                comboBoxFunction.Items.Add(str);
            }
            comboBoxFunction.SelectedIndex = 0;         
            this.Text = listFunc[0] + name;       
        }


        private void passwordGenerator()
        {
            try
            {
                int PASSWORD_LENGTH = Int32.Parse(textBoxPasswordLength.Text);

                if (PASSWORD_LENGTH < 8 || PASSWORD_LENGTH > 128)
                {
                    MessageBox.Show("Password must be at least 8 characters and not more than 128 characters", "Input invalid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {

                    bool includeAt = Convert.ToBoolean(checkBoxIncludeAt.CheckState);
                    bool includeSpecialCharacter = Convert.ToBoolean(checkBoxIncludeSpecialCharacter.Checked);

                    int scoreExpect = 3;

                    if (includeAt || includeSpecialCharacter) scoreExpect = 4;
                    if (includeAt && includeSpecialCharacter) scoreExpect = 5;

                    string password = "";
                    while (true)
                    {
                        password = generatePassword(PASSWORD_LENGTH, includeAt, includeSpecialCharacter);
                        if (validatePassword(password, scoreExpect)) break;
                    }

                    textBoxPasswordOutput.Text = password;
                }
            }catch (Exception)
            {
                MessageBox.Show("Please enter 'Length'", "Input invalid", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool validatePassword(string password, int scoreExpect)
        {
            int score = 0;
            if (password.IndexOfAny(PASSWORD_CHARS_LCASE.ToCharArray()) != -1) score++;
            if (password.IndexOfAny(PASSWORD_CHARS_UCASE.ToCharArray()) != -1) score++;
            if (password.IndexOfAny(PASSWORD_CHARS_NUMERIC.ToCharArray()) != -1) score++;
            if (password.IndexOfAny(PASSWORD_CHARS_SPECIAL.ToCharArray()) != -1) score++;
            if (password.IndexOfAny(PASSWORD_CHAR_AT.ToCharArray()) != -1) score++;

            if (score >= scoreExpect) return true;
            else return false;
        }




        private string generatePassword(int PASSWORD_LENGTH, bool includeAt, bool includeSpecialCharacter)
        {
        
            System.Random random = new System.Random();

            char[] chars = new char[PASSWORD_LENGTH];

            string validChars = PASSWORD_CHARS_LCASE + PASSWORD_CHARS_UCASE + PASSWORD_CHARS_NUMERIC;

            if (includeSpecialCharacter) validChars += PASSWORD_CHARS_SPECIAL;

            for (int i = 0; i < PASSWORD_LENGTH; i++)
            {
                chars[i] = validChars[random.Next(0, validChars.Length)];
            }

            string password = new string(chars);

            if (includeAt)
            {
                System.Random rnd = new System.Random();
                int ran = rnd.Next(1, password.Length);
                password = AppendAtPosition(password, ran, PASSWORD_CHAR_AT).Substring(0, PASSWORD_LENGTH);
            }
            return password;
        }

        private void ButtonGeneratePassword_Click(object sender, EventArgs e)
        {
           passwordGenerator();
        }

        private static string AppendAtPosition(string baseString, int position, string character)
        {
            var sb = new StringBuilder(baseString);
            for (int i = position; i < position + 1; i++)
                sb.Insert(i, character);
            return sb.ToString();
        }

        private void ComboBoxFunction_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedIndex = comboBoxFunction.SelectedIndex;

            switch (selectedIndex)
            {
                case 0:
                    panelPasswordGenerator.Visible = true;
                    panelMACGenerator.Visible = false;
                    panelPhoneNumberGenerator.Visible = false;
                    this.Text = listFunc[0] + name;
                    break;
                case 1:
                    panelPasswordGenerator.Visible = false;
                    panelMACGenerator.Visible = true;
                    panelMACGenerator.Location = new Point(12, 12);
                    panelPhoneNumberGenerator.Visible = false;
                    this.Text = listFunc[1] + name;
                    break;
                case 2:
                    panelPasswordGenerator.Visible = false;
                    panelMACGenerator.Visible = false;
                    panelPhoneNumberGenerator.Visible = true;
                    panelPhoneNumberGenerator.Location = new Point(12, 12);
                    this.Text = listFunc[2] + name;
                    break;
                default:
                    panelPasswordGenerator.Visible = true;
                    panelMACGenerator.Visible = false;
                    panelPhoneNumberGenerator.Visible = false;
                    this.Text = listFunc[0] + name;
                    break;
            }
        }

        public static string GetRandomWifiMacAddress()
        {
            var random = new System.Random();
            var buffer = new byte[6];
            random.NextBytes(buffer);
            var result = string.Concat(buffer.Select(x => string.Format("{0}", x.ToString("X2"))).ToArray());
            return result;
        }

        private void ButtonGenerateMAC_Click(object sender, EventArgs e)
        {
            string prefix = textBoxPrefix.Text;

            string mac = GetRandomWifiMacAddress();

            if(prefix != "")
            {
                mac = prefix + mac.Substring(prefix.Length , mac.Length - prefix.Length);
            }

            if (Convert.ToBoolean(checkBoxIncludeColon.CheckState))
            {
                string macTemp = "";
                int j = 0;
                for (int i = 1; i < mac.Length + 1; i++)
                {
                   if (i % 2 == 0)
                    {
                        macTemp += mac.Substring(j, 2) + ":";
                        j = i;
                    }
                }
                mac = macTemp.Substring(0, macTemp.Length -1);
            }
            textBoxMACOutput.Text = mac;
        }

        static string GetRandomTelNo(int lengh)
        {
            System.Random rand = new System.Random();
            StringBuilder telNo = new StringBuilder(lengh);
            int number;
            for (int i = 0; i < 3; i++)
            {
                number = rand.Next(0, 8); // digit between 0 (incl) and 8 (excl)
                telNo = telNo.Append(number.ToString());
            }
            telNo = telNo.Append("-");
            number = rand.Next(0, 743); // number between 0 (incl) and 743 (excl)
            telNo = telNo.Append(String.Format("{0:D3}", number));
            telNo = telNo.Append("-");
            number = rand.Next(0, 10000); // number between 0 (incl) and 10000 (excl)
            telNo = telNo.Append(String.Format("{0:D4}", number));
            return telNo.ToString();
        }

        private void ButtonGeneratePhone_Click(object sender, EventArgs e)
        {
            string phoneNumber  = GetRandomTelNo(10);
            string areCode = textBoxAreaCode.Text;
            if (areCode != "")
            {
                if(areCode.Length > 3 || areCode.Length < 2)
                {
                    MessageBox.Show("Erea code must be 3 numbers", "Input invalid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    
                }
                else
                {
                    phoneNumber = areCode + phoneNumber.Substring(2, phoneNumber.Length - areCode.Length);
                }
               
            }

            if (!Convert.ToBoolean(checkBoxIncludeHyphen.CheckState))
            {
                phoneNumber = phoneNumber.Replace("-", "");
            }

            if (Convert.ToBoolean(checkBoxIncludePlus1.CheckState))
            {
                phoneNumber = "+1" + phoneNumber;
            }

            textBoxPhoneOutput.Text = phoneNumber;
        }

        private void TextBoxPasswordOutput_DoubleClick(object sender, EventArgs e)
        {
            
            string text = textBoxPasswordOutput.Text;
            Clipboard.SetDataObject(text);
            labelAlertPasssword.Hide();
            Thread.Sleep(100);
            labelAlertPasssword.Show();
            labelAlertPasssword.Text = "Copied to clipboard";


        }

        private void TextBoxPhoneOutput_DoubleClick(object sender, EventArgs e)
        {
            string text = textBoxPhoneOutput.Text;
            Clipboard.SetDataObject(text);
            labelAlertPhoneNumber.Hide();
            Thread.Sleep(100);
            labelAlertPhoneNumber.Show();
            labelAlertPhoneNumber.Text = "Copied to clipboard";

        }

        private void TextBoxMACOutput_DoubleClick(object sender, EventArgs e)
        {
            
            string text = textBoxMACOutput.Text;
            Clipboard.SetDataObject(text);
            labelAlertMacAddress.Hide();
            Thread.Sleep(100);
            labelAlertMacAddress.Show();
            labelAlertMacAddress.Text = "Copied to clipboard";

        }


    }
}
